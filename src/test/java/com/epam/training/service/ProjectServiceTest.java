package com.epam.training.service;

import com.epam.training.model.Project;
import com.epam.training.utils.InitializationDB;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ProjectServiceTest {
    private EmployeeService employeeService = new EmployeeService();
    private ProjectService projectService = new ProjectService();
    private UnitService unitService = new UnitService();

    @Before
    public void init() {
        InitializationDB.deleteFromTables();
        InitializationDB.init();
    }

    @Test
    public void addAndFindProject() {
        Project project = new Project("testProject");
        int id = projectService.addProject(project);
        project = projectService.findProject(id);
        assertTrue(project.getProjectName().equals("testProject"));
    }

    @Test
    public void deleteProject() {
        Project project = new Project("testProject");
        int id = projectService.addProject(project);
        assertNotNull(projectService.findProject(id));
        projectService.deleteProject(id);
        assertNull(projectService.findProject(id));
    }

    @Test
    public void update() {
        Project project = new Project("testProject");
        int id = projectService.addProject(project);
        project.setProjectName("testNameProject");
        projectService.update(project, id);
        assertTrue(projectService.findProject(id).getProjectName().equals("testNameProject"));
    }
}