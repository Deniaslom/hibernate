package com.epam.training.service;

import com.epam.training.model.Project;
import com.epam.training.model.Unit;
import com.epam.training.utils.InitializationDB;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UnitServiceTest {
    private EmployeeService employeeService = new EmployeeService();
    private ProjectService projectService = new ProjectService();
    private UnitService unitService = new UnitService();

    @Before
    public void init() {
        InitializationDB.deleteFromTables();
        InitializationDB.init();
    }

    @Test
    public void addAndFindUnit() {
        Unit unit = new Unit("marketingTest");
        int id = unitService.addUnit(unit);
        unit = unitService.findUnit(id);
        assertTrue(unit.getUnitName().equals("marketingTest"));
    }

    @Test
    public void deleteUnit() {
        Unit unit = new Unit("marketingTest");
        int id = unitService.addUnit(unit);
        assertNotNull(unitService.findUnit(id));
        unitService.deleteUnit(id);
        assertNull(unitService.findUnit(id));
    }

    @Test
    public void update() {
        Unit unit = new Unit("marketingTest");
        int id = unitService.addUnit(unit);
        unit.setUnitName("testNameUnit");
        unitService.update(unit, id);
        assertTrue(unitService.findUnit(id).getUnitName().equals("testNameUnit"));
    }
}