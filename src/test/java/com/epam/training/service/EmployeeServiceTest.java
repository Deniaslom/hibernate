package com.epam.training.service;

import com.epam.training.factory.CustomSessionFactory;
import com.epam.training.model.*;
import com.epam.training.utils.InitializationDB;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeServiceTest {
    private EmployeeService employeeService = new EmployeeService();
    private ProjectService projectService = new ProjectService();
    private UnitService unitService = new UnitService();

    @Before
    public void init() {
        InitializationDB.deleteFromTables();
        InitializationDB.init();
    }

    @Test
    public void addAndFindEmployee() {
        Employee employee = InitializationDB.getTestEmployee();
        int id = employeeService.addEmployee(employee);
        Employee employee1 = employeeService.findEmployee(id);
        assertTrue(employee1.getName().equals(employee.getName()));
    }


    @Test
    public void deleteEmployee() {
        Employee employee = InitializationDB.getTestEmployee();
        int id = employeeService.addEmployee(employee);
        assertNotNull( employeeService.findEmployee(id));

        employeeService.deleteEmployee(id);
        assertNull(employeeService.findEmployee(id));
    }

    @Test
    public void update() {
        Employee employee = InitializationDB.getTestEmployee();
        employee.setName("testName123");
        int id = employeeService.addEmployee(employee);
        employeeService.update(employee, id);
        Employee employeeExpected = employeeService.findEmployee(id);
        assertTrue(employee.getName().equals(employeeExpected.getName()));
        assertTrue(employee.getAddress().equals(employeeExpected.getAddress()));
    }

    @Test
    public void assignEmployeeForUnit() {
        Employee employee = InitializationDB.getTestEmployee();
        Unit unit = new Unit("testing_db");
        int employee_id = employeeService.addEmployee(employee);
        int unit_id = unitService.addUnit(unit);
        employeeService.assignEmployeeForUnit(employee_id,unit_id);
        employee = employeeService.findEmployee(employee_id);
        assertTrue(employee.getUnit().getUnitName().equals(unitService.findUnit(unit_id).getUnitName()));
    }

//    @Test
//    public void assignEmployeeForProject() {
//        Employee employee = InitializationDB.getTestEmployee();
//        int employee_id = employeeService.addEmployee(employee);
//        Project project = new Project("testProject");
//        int project_id = projectService.addProject(project);
//
//
//        employeeService.assignEmployeeForProject(employee_id,project_id);
//        employee = employeeService.findEmployee(employee_id);
//        project = projectService.findProject(project_id);
//        System.out.println(employee);
//        System.out.println(project);
//        assertTrue(employee.getProjects().contains(project));
//    }
}