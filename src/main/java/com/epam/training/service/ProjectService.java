package com.epam.training.service;

import com.epam.training.factory.CustomSessionFactory;
import com.epam.training.model.Employee;
import com.epam.training.model.Project;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * service for working with an project for the database
 */
public class ProjectService {
    private static final Logger LOGGER = Logger.getLogger(ProjectService.class);

    /**
     * adds an employee to the database
     * @param project
     * @return project_id
     */
    public int addProject(Project project) {
        try (Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(project);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            LOGGER.log(Level.ERROR, "problem in addProject(Project project) method: " + e);
        }
        return project.getProject_id();
    }

    /**
     * takes an project from the database by id
     * @param id
     * @return employee
     */
    public Project findProject(Integer id) {
        Project project = null;
        try (Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            project = session.find(Project.class, id);
            LOGGER.info(project);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            LOGGER.log(Level.ERROR, "problem in findProject(Project id) method: " + e);
        }
        return project;
    }

    /**
     * removes an project from the database by id
     * @param id
     */
    public void deleteProject(Integer id) {
        try (Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            Project project = session.get(Project.class, id);
            for (Employee e : project.getEmployees()) {
                e.getProjects().remove(project);
                session.update(e);
            }
            project.getEmployees().clear();
            session.delete(project);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            LOGGER.log(Level.ERROR, "problem in deleteProject(Integer id) method: " + e);
        }
    }

    /**
     * changes project data in the database
     * @param model - modified employee
     * @param id - database employee
     */
    public void update(Project model, Integer id) {
        try (Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            Project project = session.get(Project.class, id);
            project.setProjectName(model.getProjectName());
            session.update(project);
            session.getTransaction().commit();
        } catch (Exception ex) {
            LOGGER.error("Error in update-method: ", ex);
        }
    }


    /**
     * a list of projects that have external employees
     * @return list of projects
     */
    public List<Project> getProjectsByExternalEmployee() {
        String queryString = "FROM Project p JOIN FETCH p.employees emp WHERE emp.external =: external";
        List<Project> projects = new ArrayList<>();
        boolean external = false;
        try (Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            Query query = session.createQuery(queryString, Project.class);
                        query.setParameter("external", external);
                        projects = query.list();
        } catch (Exception ex) {
            LOGGER.error("Error in getExternalEmployeeProject-method: ", ex);
        }
        return projects;
    }


}
