package com.epam.training.service;

import com.epam.training.factory.CustomSessionFactory;
import com.epam.training.model.Employee;
import com.epam.training.model.Project;
import com.epam.training.model.Unit;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;


/**
 * service for working with an employee for the database
 */
public class EmployeeService {
    private static final Logger LOGGER = Logger.getLogger(EmployeeService.class);

    /**
     * adds an employee to the database
     * @param employee
     * @return employee_id
     */
    public int addEmployee(Employee employee) {
        try (Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(employee);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            LOGGER.log(Level.ERROR, "problem in createEmployee(Employee employee) method: " + e);
        }
        return employee.getEmployee_id();
    }


    /**
     * takes an employee from the database
     * @param id
     * @return employee
     */
    public Employee findEmployee(Integer id) {
        Employee employee = null;
        try (Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            employee = session.find(Employee.class, id);
            LOGGER.info("findEmployee: " + employee);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            LOGGER.log(Level.ERROR, "problem in findEmployee(Integer id) method: " + e);
        }
        return employee;
    }

    /**
     * removes an employee from the database by id
     * @param id
     */
    public void deleteEmployee(Integer id) {
        try (Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            Employee employee = session.get(Employee.class, id);
            LOGGER.info("deleteEmployee: " + employee.getEmployee_id());

            session.delete(employee);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            LOGGER.log(Level.ERROR, "problem in deleteEmployee(Integer id) method: " + e);
        }
    }

    /**
     * changes employee data in the database
     * @param model - modified employee
     * @param id - database employee
     */
    public void update(Employee model, Integer id) {
        try (Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            Employee employee = session.get(Employee.class, id);
            employee.setUnit(model.getUnit());
            employee.setExternal(model.isExternal());
            employee.setName(model.getName());
            employee.setAddress(model.getAddress());
            employee.setProjects(model.getProjects());
            employee.setPersonalInfo(model.getPersonalInfo());
            employee.setStatus(model.getStatus());
            session.update(employee);
            session.getTransaction().commit();
        } catch (Exception ex) {
            LOGGER.error("Error in update(Employee model, Integer id) method: ", ex);
        }
    }

    /**
     * adding an employee to the unit by idUnit
     * @param employeeId
     * @param unitId
     */
    public void assignEmployeeForUnit(int employeeId, int unitId) {
        try (Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            Employee employee = session.get(Employee.class, employeeId);
            Unit unit = session.get(Unit.class, unitId);
            unit.addEmployee(employee);
            employee.setUnit(unit);
            session.update(unit);
            session.update(employee);
            session.getTransaction().commit();
        } catch (Exception ex) {
            LOGGER.error("Error in assignEmployeeForUnit(int employeeId, int unitId) method: ", ex);
        }
    }

    /**
     * Adding an employee to an ID project
     * @param employeeId
     * @param projectId
     */
    public void assignEmployeeForProject(int employeeId, int projectId) {
        try (Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            Employee employee = session.find(Employee.class, employeeId);
            Project project = session.find(Project.class, projectId);
            employee.getProjects().add(project);
            project.getEmployees().add(employee);
            session.update(employee);
            session.update(project);
            session.getTransaction().commit();
        } catch (Exception ex) {
            LOGGER.error("Error in assignEmployeeForProject method: ", ex);
        }
    }
}
