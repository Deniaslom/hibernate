package com.epam.training.service;

import com.epam.training.factory.CustomSessionFactory;
import com.epam.training.model.Unit;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 * service for working with an unit for the database
 */
public class UnitService {
    private static final Logger LOGGER = Logger.getLogger(UnitService.class);

    /**
     * adds an employee to the database
     * @param unit
     * @return unit_id
     */
    public int addUnit(Unit unit){
        try(Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(unit);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            LOGGER.log(Level.ERROR, "problem in addUnit(Unit unit) method: " + e);
        }
        return unit.getUnit_id();
    }

    /**
     * takes an unit from the database by id
     * @param id
     * @return employee
     */
    public Unit findUnit(Integer id) {
        Unit unit = null;
        try(Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            unit = session.find(Unit.class, id);
            LOGGER.info("findUnit: " + unit);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            LOGGER.log(Level.ERROR, "problem in findUnit(Unit id) method: " + e);
        }
        return unit;
    }

    /**
     * removes an unit from the database by id
     * @param id
     */
    public void deleteUnit(Integer id) {
        try(Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            Unit unit = session.get(Unit.class, id);
            session.delete(unit);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            LOGGER.log(Level.ERROR, "problem in deleteEmployee(Integer id) method: " + e);
        }
    }

    /**
     * changes unit data in the database
     * @param model - modified employee
     * @param id - database employee
     */
    public void update(Unit model, Integer id) {
        try (Session session = CustomSessionFactory.getSessionFactory().openSession()) {
            session.beginTransaction();
            Unit unit = session.get(Unit.class, id);
            unit.setUnitName(model.getUnitName());
            session.update(unit);
            session.getTransaction().commit();
        } catch (Exception ex) {
            LOGGER.error("Error in update-method: ", ex);
        }
    }
}
