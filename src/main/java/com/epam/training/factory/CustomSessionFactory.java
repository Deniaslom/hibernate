package com.epam.training.factory;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

/**
 * class for creating a session
 */
public class CustomSessionFactory {
    private static final Logger LOGGER = Logger.getLogger(CustomSessionFactory.class);

    private static StandardServiceRegistry serviceRegistry;
    private static SessionFactory sessionFactory;

    private CustomSessionFactory(){

    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                serviceRegistry = new StandardServiceRegistryBuilder()
                        .configure()
                        .build();
                sessionFactory = new MetadataSources(serviceRegistry)
                        .buildMetadata()
                        .buildSessionFactory();
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
                if (serviceRegistry != null) {
                    StandardServiceRegistryBuilder.destroy(serviceRegistry);
                }
            }
        }
        return sessionFactory;
    }

    public static void shutdown() {
        if (serviceRegistry != null) {
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
    }
}