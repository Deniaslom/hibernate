package com.epam.training.utils;

import com.epam.training.factory.CustomSessionFactory;
import com.epam.training.model.*;
import com.epam.training.service.EmployeeService;
import com.epam.training.service.ProjectService;
import com.epam.training.service.UnitService;
import org.hibernate.Session;


public class InitializationDB {
    private static EmployeeService employeeService = new EmployeeService();
    private static UnitService unitService = new UnitService();
    private static ProjectService projectService = new ProjectService();

    public static void init() {


        Unit unit1 = new Unit("testing");
        Unit unit2 = new Unit("programmer");
        Unit unit3 = new Unit("servers");

        Employee employee1 = new Employee("Denis", true, EmployeeStatus.ACTIVE, new Address("Jukova1", 52), new EmployeePersonalInfo("Den", "123"));
        Employee employee2 = new Employee("James222", false, EmployeeStatus.ACTIVE, new Address("Pesina1", 8), new EmployeePersonalInfo("Maks", "234"));
        Employee employee3 = new Employee("James322", true, EmployeeStatus.PASSIVE, new Address("Kirova1", 10), new EmployeePersonalInfo("Nikita", "345"));
        Employee employee4 = new Employee("James422", false, EmployeeStatus.PASSIVE, new Address("Sovetckaiy1", 23), new EmployeePersonalInfo("Slava", "456"));

        Project project1 = new Project("project1");
        Project project2 = new Project("project2");
        Project project3 = new Project("project3");
        Project project4 = new Project("project4");
        Project project5 = new Project("project5");
        Project project6 = new Project("project6");
        Project project7 = new Project("project7");

        projectService.addProject(project1);
        projectService.addProject(project2);
        projectService.addProject(project3);
        projectService.addProject(project4);
        projectService.addProject(project5);
        projectService.addProject(project6);
        projectService.addProject(project7);

        unitService.addUnit(unit1);
        unitService.addUnit(unit2);
        unitService.addUnit(unit3);

        employee1.getProjects().add(project1);
        employee2.getProjects().add(project1);
        employee3.getProjects().add(project1);
        employee4.getProjects().add(project1);

        employee2.getProjects().add(project2);
        employee3.getProjects().add(project2);
        employee4.getProjects().add(project2);

        employee3.getProjects().add(project3);
        employee4.getProjects().add(project3);

        employee4.getProjects().add(project4);

        employee1.setUnit(unit1);
        employee2.setUnit(unit2);
        employee3.setUnit(unit3);
        employee4.setUnit(unit3);

        employeeService.addEmployee(employee1);
        employeeService.addEmployee(employee2);
        employeeService.addEmployee(employee3);
        employeeService.addEmployee(employee4);

    }

    public static void deleteFromTables(){
        String queryString1 = "DELETE from Employee";
        String queryString2 = "DELETE from Unit";
        String queryString3 = "DELETE from Project";
        String queryString4 = "DELETE from EmployeePersonalInfo ";

        Session session = CustomSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.createQuery(queryString1).executeUpdate();
        session.createQuery(queryString2).executeUpdate();
        session.createQuery(queryString3).executeUpdate();
        session.createQuery(queryString4).executeUpdate();
        session.getTransaction().commit();
    }

    public static Employee getTestEmployee(){
        Employee employee = new Employee("testName", false, EmployeeStatus.ACTIVE, new Address("Pesina", 52), new EmployeePersonalInfo("Den", "12345678"));
        Unit unit1 = new Unit("testing");
        Project project1 = new Project("project1");
        projectService.addProject(project1);
        unitService.addUnit(unit1);

        employee.getProjects().add(project1);
        employee.setUnit(unit1);

        return employee;
    }


}
