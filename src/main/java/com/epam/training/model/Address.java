package com.epam.training.model;


import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
public class Address {


    private String street;
    private int building;

    public Address() {
    }

    public Address(String street, int building) {
        this.street = street;
        this.building = building;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getBuilding() {
        return building;
    }

    public void setBuilding(int building) {
        this.building = building;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return building == address.building &&
                Objects.equals(street, address.street);
    }

    @Override
    public int hashCode() {
        return Objects.hash(street, building);
    }

    @Override
    public String toString() {
        return "Address{" +
                ", street='" + street + '\'' +
                ", building=" + building +
                '}';
    }
}
