package com.epam.training.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class EmployeePersonalInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String telephoneNumber;

    @OneToOne(mappedBy = "personalInfo")
    private Employee employee;


    public EmployeePersonalInfo() {
    }

    public EmployeePersonalInfo(String name, String telephoneNumber) {
        this.name = name;
        this.telephoneNumber = telephoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, telephoneNumber);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeePersonalInfo that = (EmployeePersonalInfo) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(telephoneNumber, that.telephoneNumber) &&
                Objects.equals(employee, that.employee);
    }


    @Override
    public String toString() {
        return "EmployeePersonalInfo{" +
                "name='" + name + '\'' +
                ", telephoneNumber='" + telephoneNumber + '\'' +
                '}';
    }
}
