package com.epam.training.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer employee_id;

    private String name;
    private boolean external;
    private EmployeeStatus status;

    @Embedded
    private Address address;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "employee_id")
    private EmployeePersonalInfo personalInfo;

    @ManyToMany
    @JoinTable(
            name = "employee_project",
            joinColumns = {@JoinColumn(name = "employee_id")},
            inverseJoinColumns = {@JoinColumn(name = "project_id")}
    )
    private Set<Project> projects = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "unit_table_id", nullable = false)
    private Unit unit;

    public Employee() {
    }

    public Employee(String name, boolean external, EmployeeStatus status, Address address, EmployeePersonalInfo personalInfo) {
        this.name = name;
        this.external = external;
        this.status = status;
        this.address = address;
        this.personalInfo = personalInfo;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public boolean isExternal() {
        return external;
    }

    public void setExternal(boolean external) {
        this.external = external;
    }

    public Integer getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(Integer employee_id) {
        this.employee_id = employee_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EmployeeStatus getStatus() {
        return status;
    }

    public void setStatus(EmployeeStatus status) {
        this.status = status;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public EmployeePersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(EmployeePersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    @Override
    public int hashCode() {
        return Objects.hash(employee_id, name, external, status, address, personalInfo, projects, unit);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return external == employee.external &&
                Objects.equals(name, employee.name) &&
                status == employee.status &&
                Objects.equals(address, employee.address) &&
                Objects.equals(personalInfo, employee.personalInfo) &&
                Objects.equals(projects, employee.projects) &&
                Objects.equals(unit, employee.unit);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employee_id=" + employee_id +
                ", name='" + name + '\'' +
                ", external=" + external +
                ", status=" + status +
                ", address=" + address +
                ", personalInfo=" + personalInfo +
                ", projects=" + projects +
                ", unit=" + unit +
                '}';
    }
}
